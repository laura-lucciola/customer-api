﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CustomerAPI.Common
{
    public class Customer
    {
        public Guid CustomerId { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 1)]
        public string Title { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string Forename { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string Surname { get; set; }

        [Required]
        [StringLength(75, MinimumLength = 1)]
        public string Email { get; set; }

        [Required]
        [StringLength(15, MinimumLength = 1)]
        public string Mobile { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }

        public Customer()
        {
            CustomerId = Guid.NewGuid();            
        }

        public Customer(string Title, string Forename, string Surname, string Email, string Mobile) :this ()
        {

            this.Title = Title;
            this.Forename = Forename;
            this.Surname = Surname;
            this.Email = Email;
            this.Mobile = Mobile;
        }
        public Customer(Guid CustomerId, string Title, string Forename, string Surname, string Email, string Mobile) : this(Title, Forename, Surname, Email, Mobile)
        {
            this.CustomerId = CustomerId;
        }

        public bool CompareTo(object obj)
        {
            if (obj is Customer stuObj)
            {
                return Title.Equals(stuObj.Title) &&
                        Forename.Equals(stuObj.Forename) &&
                        Surname.Equals(stuObj.Surname) &&
                        Email.Equals(stuObj.Email) &&
                        Mobile.Equals(stuObj.Mobile);
            }
            return false;
        }
    }
}