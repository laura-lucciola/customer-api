using System;
using System.ComponentModel.DataAnnotations;
using Xunit;

namespace CustomerAPI.Tests
{
    public class CustomerTests
    {
        [Fact]
        public void ShouldAllowValidCustomer()
        {
            var customer = TestUtils.CreateTestCustomer();

            TestUtils.GetValidaTionResult(customer, out bool valid, out ValidationResult firstValidationResult);

            Assert.True(valid);
            Assert.Null(firstValidationResult);
        }

        [Theory]
        [InlineData(0, "The Title field is required.")]
        [InlineData(21, "The field Title must be a string with a minimum length of 1 and a maximum length of 20.")]
        public void ShouldNotAllowCustomerWithInvalidTitle(int fieldLength, string validationResult)
        {
            var sut = TestUtils.CreateInvalidField(fieldLength);
            var customer = TestUtils.CreateTestCustomer();
            customer.Title = sut;

            TestUtils.GetValidaTionResult(customer, out bool valid, out ValidationResult firstValidationResult);

            Assert.Equal(validationResult, firstValidationResult.ErrorMessage);
            Assert.False(valid);
        }

        [Theory]
        [InlineData(0, "The Forename field is required.")]
        [InlineData(51, "The field Forename must be a string with a minimum length of 1 and a maximum length of 50.")]
        public void ShouldNotAllowCustomerWithInvalidForename(int fieldLength, string validationResult)
        {
            var sut = TestUtils.CreateInvalidField(fieldLength);
            var customer = TestUtils.CreateTestCustomer();
            customer.Forename = sut;

            TestUtils.GetValidaTionResult(customer, out bool valid, out ValidationResult firstValidationResult);

            Assert.Equal(validationResult, firstValidationResult.ErrorMessage);
            Assert.False(valid);
        }

        [Theory]
        [InlineData(0, "The Surname field is required.")]
        [InlineData(51, "The field Surname must be a string with a minimum length of 1 and a maximum length of 50.")]
        public void ShouldNotAllowCustomerWithInvalidSurname(int fieldLength, string validationResult)
        {
            var sut = TestUtils.CreateInvalidField(fieldLength);
            var customer = TestUtils.CreateTestCustomer();
            customer.Surname = sut;

            TestUtils.GetValidaTionResult(customer, out bool valid, out ValidationResult firstValidationResult);

            Assert.Equal(validationResult, firstValidationResult.ErrorMessage);
            Assert.False(valid);
        }

        [Theory]
        [InlineData(0, "The Email field is required.")]
        [InlineData(76, "The field Email must be a string with a minimum length of 1 and a maximum length of 75.")]
        public void ShouldNotAllowCustomerWithInvalidEmail(int fieldLength, string validationResult)
        {
            var sut = TestUtils.CreateInvalidField(fieldLength);
            var customer = TestUtils.CreateTestCustomer();
            customer.Email = sut;

            TestUtils.GetValidaTionResult(customer, out bool valid, out ValidationResult firstValidationResult);

            Assert.Equal(validationResult, firstValidationResult.ErrorMessage);
            Assert.False(valid);
        }

        [Theory]
        [InlineData(0, "The Mobile field is required.")]
        [InlineData(21, "The field Mobile must be a string with a minimum length of 1 and a maximum length of 15.")]
        public void ShouldNotAllowCustomerWithInvalidMobile(int fieldLength, string validationResult)
        {
            var sut = TestUtils.CreateInvalidField(fieldLength);
            var customer = TestUtils.CreateTestCustomer();
            customer.Mobile = sut;

            TestUtils.GetValidaTionResult(customer, out bool valid, out ValidationResult firstValidationResult);

            Assert.Equal(validationResult, firstValidationResult.ErrorMessage);
            Assert.False(valid);
        }

        [Fact]
        public void ShouldGenerateCustomId()
        {
            var customer = TestUtils.CreateTestCustomer();

            var result = customer.CustomerId;

            Assert.IsType<Guid>(result);
        }
    }
}
