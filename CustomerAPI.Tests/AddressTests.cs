using CustomerAPI.Common;
using System;
using System.ComponentModel.DataAnnotations;
using Xunit;

namespace CustomerAPI.Tests
{
    public class AddressTests
    {
        [Fact]
        public void ShouldAllowValidFullAddress()
        {
            var address = TestUtils.CreateTestAddress();

            TestUtils.GetValidaTionResult(address, out bool valid, out ValidationResult firstValidationResult);

            Assert.True(valid);
            Assert.Null(firstValidationResult);
        }

        [Fact]
        public void ShouldAllowValidAddress()
        {
            var address = TestUtils.CreateTestAddress();
            address.AddressLine2 = null;
            address.County = null;
            address.Country = null;

            TestUtils.GetValidaTionResult(address, out bool valid, out ValidationResult firstValidationResult);

            Assert.True(valid);
            Assert.Null(firstValidationResult);
        }

        [Theory]
        [InlineData(0, "The AddressLine1 field is required.")]
        [InlineData(81, "The field AddressLine1 must be a string with a minimum length of 1 and a maximum length of 80.")]
        public void ShouldNotAllowAddressWithInvalidAddressLine1(int fieldLength, string validationResult)
        {
            var sut = TestUtils.CreateInvalidField(fieldLength);
            var address = TestUtils.CreateTestAddress();
            address.AddressLine1 = sut;

            TestUtils.GetValidaTionResult(address, out bool valid, out ValidationResult firstValidationResult);

            Assert.Equal(validationResult, firstValidationResult.ErrorMessage);
            Assert.False(valid);
        }

        [Theory]
        [InlineData(81, "The field AddressLine2 must be a string with a maximum length of 80.")]
        public void ShouldNotAllowAddressWithInvalidAddressLine2(int fieldLength, string validationResult)
        {
            var sut = TestUtils.CreateInvalidField(fieldLength);
            var address = TestUtils.CreateTestAddress();
            address.AddressLine2 = sut;

            TestUtils.GetValidaTionResult(address, out bool valid, out ValidationResult firstValidationResult);

            Assert.Equal(validationResult, firstValidationResult.ErrorMessage);
            Assert.False(valid);
        }

        [Theory]
        [InlineData(0, "The Town field is required.")]
        [InlineData(51, "The field Town must be a string with a minimum length of 1 and a maximum length of 50.")]
        public void ShouldNotAllowAddressWithInvalidTown(int fieldLength, string validationResult)
        {
            var sut = TestUtils.CreateInvalidField(fieldLength);
            var address = TestUtils.CreateTestAddress();
            address.Town = sut;

            TestUtils.GetValidaTionResult(address, out bool valid, out ValidationResult firstValidationResult);

            Assert.Equal(validationResult, firstValidationResult.ErrorMessage);
            Assert.False(valid);
        }

        [Theory]
        [InlineData(51, "The field County must be a string with a maximum length of 50.")]
        public void ShouldNotAllowAddressWithInvalidCounty(int fieldLength, string validationResult)
        {
            var sut = TestUtils.CreateInvalidField(fieldLength);
            var address = TestUtils.CreateTestAddress();
            address.County = sut;

            TestUtils.GetValidaTionResult(address, out bool valid, out ValidationResult firstValidationResult);

            Assert.Equal(validationResult, firstValidationResult.ErrorMessage);
            Assert.False(valid);
        }

        [Theory]
        [InlineData(0, "The Postcode field is required.")]
        [InlineData(11, "The field Postcode must be a string with a minimum length of 1 and a maximum length of 10.")]
        public void ShouldNotAllowAddressWithInvalidPostcode(int fieldLength, string validationResult)
        {
            var sut = TestUtils.CreateInvalidField(fieldLength);
            var address = TestUtils.CreateTestAddress();
            address.Postcode = sut;

            TestUtils.GetValidaTionResult(address, out bool valid, out ValidationResult firstValidationResult);

            Assert.Equal(validationResult, firstValidationResult.ErrorMessage);
            Assert.False(valid);
        }

        [Fact]
        public void ShouldSetTheCountryWhenNoneProvided()
        {
            var address = new Address("Comfy", "house", "Shire", "Central Eriador", "LO TR");

            TestUtils.GetValidaTionResult(address, out bool valid, out ValidationResult firstValidationResult);

            Assert.Equal(Constants.DefaultCountry, address.Country);
            Assert.True(valid);
            Assert.Null(firstValidationResult);
        }

        [Fact]
        public void ShouldGenerateCustomId()
        {
            var address = TestUtils.CreateTestAddress();

            var result = address.AddressId;

            Assert.IsType<Guid>(result);
        }
    }
}
