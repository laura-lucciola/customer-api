﻿using CustomerAPI.Common;
using CustomerAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CustomerAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerManager _manager;
        public CustomerController(ICustomerManager manager)
        {
            _manager = manager;
        }

        [HttpGet("Ping")]
        public string Ping()
        {
            return DateTime.Now.ToShortTimeString();
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var customers = _manager.GetAllCustomers();
            return Ok(customers);
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] Guid id)
        {
            try
            {
                var customer = _manager.GetCustomerById(id);
                return Ok(customer);
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(new { title = e.Message });
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] Customer customer)
        {
            try
            {
                var result = _manager.CreateCustomer(customer);
                return Created("GetCustomer", new { id = result });
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(new { title = e.Message });
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            try
            {
                _manager.DeleteCustomer(id);
                return NoContent();
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(new { title = e.Message });
            }
        }
    }
}
